### 1.6.0

* Added Book 6 maps, walls and doors

    #### Book 6
    * Under Fort Tempest
    * Terimor's Tower
    * The Graveraker

### 1.5.0

* Added Book 5 maps, walls and doors

    #### Book 5
    * Bottles & Blots
    * Clockwork Halls - Lowers
    * Clockwork Halls - Upper
    * Eyes of Absalom HQ
    * House of Planes
    * Mother Venom's Lair
    * The Black Whale (100px version)
    * The Black Whale (50px version, no scene)
    * The Straight Edge

### 1.4.0

* Added Book 4 maps, walls and doors

    #### Book 4
    * Sanctuary
    * Temple
    * Lower Temple
    * Lodge / Attic / Basement
    * Fairground

### 1.3.0

* Added Book 3 maps, walls and doors

    #### Book 3
    * Arrowhead Slipway
    * Back Alley
    * Balneary
    * Lucky Nimbus
    * Sweeper School

### 1.2.7
* Updated map graphics for Pagoda, Dreaming Palace and The Catacombs. 
### 1.2.6
* Updated various wall properties, and added missing doors. Thanks Freeman!
### 1.2.5
* Added Multi-Level Token areas. Work in progress!
* Fixed some dodgy walls
### 1.2.4
* Adjusted walls on Sphinx & Coin map - feedback from Freeman
### 1.2.3
* Fixed missing image file
### 1.2.2
* Updated book 2 pack file
### 1.2.1
 
* Fixed missing pack info in module.json  
* Added map

    #### Book 2
    * Orvington Moneychangers

# 1.2.0
* Added Book 2 maps

    #### Book 2
    * The Catacombs
    * Copper Hand Hideout
    * Smuggler's Lair
    * Stonesworn Savings & Loan
    * Sphinx & Coin

    #### Book 1
    * Added Mwangi room trap tile to /maps

### 1.1.4
* Updated walls.
### 1.1.3
* Updated walls.
### 1.1.2
* Updated walls. 
### 1.1.1
* Fixed map image paths
# 1.1.0

* Initial release

    #### Book 1
    * The Dreaming Palace
    * The Menagerie
    * The Backdoor
    * The Dragonfly Pagoda
    * Tipsy Tengu 
